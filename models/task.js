const bookshelf = require('../config/bookshelf');

const Task = bookshelf.Model.extend({
    tableName: "tasks",
    hasTimestamps: true,
    autoIncrement: true,
    attachments() {
        return this.morphMany('Attachment', 'attachable', ['entity_type', 'entity_id'], 'Task');
   }
})

module.exports = bookshelf.model("Task", Task);