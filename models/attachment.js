const bookshelf = require('./../config/bookshelf');
//TODO  PLS added the updated package
//const awsUtil = require("../lib/utils/aws.util");

const _ = require('lodash');

const Attachment = bookshelf.Model.extend({
    tableName: 'attachments',
    hasTimestamps: true,
    autoIncrement: true,
    initialize: function () {
        this.on('fetched', async function (model, attributes, options) {
            //Fetching photo urls
            if (!attributes.url) {
                attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
            } else {
                const result = attributes.url.substr(0, 5);
                if (result === 'https')
                    attributes.photo_urls = await awsUtil.generateVersionNormalUrl(attributes.url);
                else {
                    attributes.photo_urls = await awsUtil.generateVersionUrls(process.env.AWS_PUBLIC_URL + attributes.url);
                }
            }
        });
        this.on('fetched:collection', async function (model, attributes, options) {
            //Fetching photo urls
            if (model.models) {
                await asyncForEach(model.models, async innerModel => {
                    if (!innerModel.attributes.url) {
                        innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
                    } else {
                        const result = innerModel.attributes.url.substr(0, 5);
                        if (result === 'https')
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl(innerModel.attributes.url);
                        else {
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionUrls(process.env.AWS_PUBLIC_URL + innerModel.attributes.url);
                        }
                    }
                });
            }
        });
    },
    attachable() {
        return this.morphTo('attachable', ['entity_type', 'entity_id'], 'User','Task');
    }
});

Attachment.prototype.toJSON = function () {
    const that = this.attributes;
    return _.omit(that, ['sort', 'created_at', 'updated_at', 'entity_type', 'entity_id']);
};

module.exports = bookshelf.model('Attachment', Attachment);