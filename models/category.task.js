const bookshelf = require('../config/bookshelf');
const CategoryTask = bookshelf.Model.extend({
    tableName: "categories_tasks",
    hasTimestamps: true,
    autoIncrement: true,
    category: function() {
        return this.hasMany('Category');
    },
    task: function() {
        return this.hasMany('Task');
    },
  
  
})

module.exports = bookshelf.model("CategoryTask", CategoryTask);