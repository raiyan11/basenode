const bookshelf = require('../config/bookshelf');

const Category = bookshelf.Model.extend({
    tableName: "categories",
    hasTimestamps: true,
    autoIncrement: true,
 
})

module.exports = bookshelf.model("Category", Category);