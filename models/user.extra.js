const bookshelf = require('../config/bookshelf');

const UserExtra = bookshelf.Model.extend({
    tableName: "user_extras",
    hasTimestamps: true,
    autoIncrement: true,
 
})

module.exports = bookshelf.model("UserExtra", UserExtra);