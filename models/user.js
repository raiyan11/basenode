const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
// can you pls added in latest pakage [aws and bcrypt]

const bcrypt = require('bcrypt');
const {bcryptConfig} = require('../config');
//const awsUtil = require("../lib/utils/aws.util");

const User = bookshelf.Model.extend({
    tableName: 'users',
    hasTimestamps: true,
    autoIncrement: true,
    initialize: function () {
        this.on('saving', function (model, attributes, options) {
            
            //TODO uncomment once bycrypt is added
            if (attributes.password && this.hasChanged()) {
                attributes.password = bcrypt.hashSync(attributes.password, bcryptConfig.hashRound);
            }
            if (!attributes.verification_token) {
                let otp = Math.floor(1000 + Math.random() * 9000);
                attributes.verification_token = otp;
            }
        });
        this.on('fetched', async function (model, attributes, options) {

             //Relations helper
             if (model.relations) {
                const obj = model.relations;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        model.attributes[key] = model.related(key).toJSON();
                    }
                }
            }

            //Fetching image urls
            // if (!attributes.photo_url) {
            //     // attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
            // } else {
            //     const result = attributes.photo_url.substr(0, 5);
            //     if (result === 'https')
            //         attributes.photo_urls = await awsUtil.generateVersionNormalUrl(attributes.photo_url);
            //     else {
            //         attributes.photo_urls = await awsUtil.generateVersionUrls(process.env.AWS_PUBLIC_URL + attributes.photo_url);
            //     }
            // }
        });
        this.on('fetched:collection', async function (model, attributes, options) {
            //Fetching image urls
            // if (model.models) {
            //     await asyncForEach(model.models, async innerModel => {
            //         if (!innerModel.attributes.photo_url) {
            //             innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
            //         } else {
            //             const result = innerModel.attributes.photo_url.substr(0, 5);
            //             if (result === 'https')
            //                 innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl(innerModel.attributes.photo_url);
            //             else {
            //                 innerModel.attributes.photo_urls = await awsUtil.generateVersionUrls(process.env.AWS_PUBLIC_URL + innerModel.attributes.photo_url);
            //             }
            //         }
            //     });
            // }
        });
    },
    userextra: function() {
        return this.hasOne('UserExtra');
    },
    attachments() {
        return this.morphMany('Attachment', 'attachable', ['entity_type', 'entity_id'], 'User');
   }
});

User.prototype.toJSON = function () {
    const that = this.attributes;
    return _.omit(that, ['password', 'reset_password_token', 'reset_password_sent_at', 'verification_token']);
};


module.exports = bookshelf.model('User', User);