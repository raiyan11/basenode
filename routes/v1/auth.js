const express = require('express');
const router = express.Router();

const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');

const signinComponent = require('../../components/v1/auth/signin');
const signUpComponent = require('../../components/v1/auth/signup');
router.post('/signin', joiMiddleware.joiBodyMiddleware(joiSchemas.signUser, 'user'), signinComponent);
router.post('/signup', joiMiddleware.joiBodyMiddleware(joiSchemas.signUp, 'user'), signUpComponent);

const forgotPasswordComponent = require('../../components/v1/auth/forgot.password');
const resetPasswordComponent = require('../../components/v1/auth/reset.password');
router.post('/password/forgot', forgotPasswordComponent);
router.put('/password/reset', resetPasswordComponent);


module.exports = router;