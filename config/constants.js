"use strict";

const constants = Object.freeze({
  userRole: {
    admin: "admin",
    user: "user",
  },
  approvalStatus: {
    notSubmitted: "not_submitted",
    requested: "requested",
    accepted: "accepted",
    denied: "denied",
  },
  error: {
    auth: {
      cannotDeleteAdmin: "Cannot delete admin",
      HashtagAlreadyExists: "Hashtag already exists",
      HashtagAwaitingApproval:
        "Hashtag already exists and is awaiting approval",
      hashtagNotExists: "Hashtag does not exist",
      emailTaken: "Email has already been taken",
      userNameTaken: "UserName has already been taken.",
      phoneNumberTaken: "Number has already been taken",
      codeMismatch: "Verification code mismatch",
      invalidToken: "Invalid Token",
      invalidCredentials: "Invalid user credentials",
      invalidUser: "Invalid user",
      userNotFound: "User not found",
      sameUser: "Cannot send request to self",
      userNotVerified: "User not verified",
      userProfileNotFound: "User Profile not found",
      deckNotFound: "Deck Not Found",
      unauthorized: "Unauthorized",
      noAuthToken: "No auth token",
      profileNotFound: "Profile not found",
      invalidAuthToken: "Invalid auth token",
      deckServiceCheck: "All ready DeckService Exit",
      passwordNotMatch: "Confirm Password does not match.",
      passwordWrong: "Current Password is wrong.",
      invalidPasswordToken: "Invalid Password Token",
      invalidEmailToken: "Invalid Email Token",
      emailNotVerified: "Email not verified",
      harmonyNotfound: "harmony not found",
      harmonyConfig: "harmony Config Worng",
      accessDenied: "Access Denied",
      userNotSame: "Cannot signup with this user. Please check user id",
      userNameNotPresent: "Username not sent",
      userNameNotAvailable: "Username not available as it is already taken.",
      blockUser: "You have not blocked the user",
      Followuser: "You are not following the user",
      wrongFollowuser: "Followuser user wrong..!",
      nothingToUpdate: "Nothing to update",
      followNotFound: "Follow request not found.",
      requestAlreadySent: "you have already requested this User.",
      requestAlreadyAccepted: "you have already accepted by this User.",
      wrongBlockUser: "User to block not found!",
      userAlreadyBlocked: "User already blocked",
      followNotFound: "Follow request not found.",
      blockedByUser: "You have been blocked by this user",
      approvalRequested: "Approval has already been requested",
      approvalAccepted: "Approval has already been accepted",
      approvalDenied: "Approval has already been denied",
      approvalNotFound: "User has not requested for approval",
      userDeactivated:
        "User Is Deactivated. Please activate to perform this action",
      notificationNotFound: "Notification not found",
      codeNotFound: "Code Not Found.",
      codeAlreadySent: "Code is already sent to this email",
      userIsNotPublic: "Requested user is not public",
    },
  },

  post: {
    hashtagNotFound: "Hashtag not found",
    hashtagsInvalid: "One or more hashtag is invalid",
    hashtagNotFoundOrApproved: "Hashtag not found or already approved",
    accountNotApproved: "Your account is not approved",
    noHashTag: "Please use atleast one hashtag",
    notFound: "Post not Found",
    alreadyLikedByUser: "Post already liked by this user",
    alreadyDislikedByUser: "Post not liked by user",
    alreadyReportedByUser: "Post already reported by this user",
    notBelongToUser: "Post does not belong to user",
    duplicateRepost: "Already reposted this post by this user",
    cannotRepostOwn: "Cannot repost own post",
    cannotRepostPrivate: "Cannot repost private user post",
    cannotUndoOtherRepost: "Cannot undo other user repost",
    postNotUsers: "Post does not belong to your user",
    cannotUpdateRepost: "Cannot update a reposted post",
    privatePost: "This post is not publicly available",
    notRepost: "This post is not a repost",
    notRepostable: "Reposting has been blocked by post owner",
  },

  activeStatus: {
    active: "active",
    inactive: "inactive",
    deleted: "deleted",
    hidden: "hidden",
  },
  requestStatus: {
    requested: "requested",
    accepted: "accepted",
    denied: "denied",
  },
  followStatus: {
    not_requested: "not_requested",
    requested: "requested",
    accepted: "accepted",
    denied: "denied",
  },
  privacySetting: {
    public: "public",
    private: "private",
  },
  postType: {
    text: "text",
    image: "image",
    repost: "repost",
  },
  userFeedFields: [
    "id",
    "user_name",
    "photo_url",
    "height",
    "width",
    "header_width",
    "header_height",
    "header_image",
    "privacy_settings",
  ],
  userFollowFields: [
    "id",
    "user_name",
    "photo_url",
    "height",
    "width",
    "header_width",
    "header_height",
    "header_image",
    "privacy_settings",
  ],
  postgressErrorCodes: [
    "42000", //SYNTAX ERROR OR ACCESS RULE VIOLATION
    "42601", //SYNTAX ERROR
    "42501", //INSUFFICIENT PRIVILEGE
    "42846", //CANNOT COERCE
    "42803", //GROUPING ERROR
    "42830", //INVALID FOREIGN KEY
    "42602", //INVALID NAME
    "42622", //NAME TOO LONG
    "42939", //RESERVED NAME
    "42804", //DATATYPE MISMATCH
    "42P18", //INDETERMINATE DATATYPE
    "42809", //WRONG OBJECT TYPE
    "42703", //UNDEFINED COLUMN
    "42883", //UNDEFINED FUNCTION
    "42P01", //UNDEFINED TABLE
    "42P02", //UNDEFINED PARAMETER
    "42704", //UNDEFINED OBJECT
    "42701", //DUPLICATE COLUMN
    "42P03", //DUPLICATE CURSOR
    "42P04", //DUPLICATE DATABASE
    "42723", //DUPLICATE FUNCTION
    "42P05", //DUPLICATE PREPARED STATEMENT
    "42P06", //DUPLICATE SCHEMA
    "42P07", //DUPLICATE TABLE
    "42712", //DUPLICATE ALIAS
    "42710", //DUPLICATE OBJECT
    "42702", //AMBIGUOUS COLUMN
    "42725", //AMBIGUOUS FUNCTION
    "42P08", //AMBIGUOUS PARAMETER
    "42P09", //AMBIGUOUS ALIAS
    "42P10", //INVALID COLUMN REFERENCE
    "42611", //INVALID COLUMN DEFINITION
    "42P11", //INVALID CURSOR DEFINITION
    "42P12", //INVALID DATABASE DEFINITION
    "42P13", //INVALID FUNCTION DEFINITION
    "42P14", //INVALID PREPARED STATEMENT DEFINITION
    "42P15", //INVALID SCHEMA DEFINITION
    "42P16", //INVALID TABLE DEFINITION
    "42P17", //INVALID OBJECT DEFINITION
  ],
});

module.exports = constants;
