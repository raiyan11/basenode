const {
    ErrorHandler
} = require('../lib/utils');
const {
    constants
} = require('../config');

module.exports.joiQueryMiddleware = async (schema, key) => {
    return (req, res, next) => {
        try {
            let requestBody = req.query;
            if (key) {
                requestBody = req.query[key];
            }
            try {
                const value = schema.validateAsync(requestBody);
                console.log('schemavalidate value', value)
                next();
            } catch (error) {
                res.serverError(400, ErrorHandler(error));
            }

        } catch (err) {
            console.log('error', ErrorHandler(err));
            res.serverError(400, ErrorHandler(error));
        }
    };
};

module.exports.joiBodyMiddleware = (schema, key) => {
    return (req, res, next) => {
        try {
            let requestBody = req.body;
            if (key) {
                requestBody = req.body[key];
            }
            if (!requestBody) {
                res.serverError(400, ErrorHandler(constants.error.bodyEmpty));
            } else {
                try {
                    const value = schema.validateAsync(requestBody);
                    console.log('schemavalidate value', value)
                    next();
                } catch (error) {
                    res.serverError(400, ErrorHandler(error));
                }
            }
        } catch (err) {
            console.log('error', err);
            res.serverError(404, {
                error: ErrorHandler(err)
            });
        }
    };
};