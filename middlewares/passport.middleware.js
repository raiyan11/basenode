const passport = require('passport');
let LocalStratergy = require('passport-local').Strategy;
const {
    jwtConfig,
    constants
} = require('../config');
const {
    jwtUtil,
    ErrorHandler,
    // awsUtil,
    twilioUtil
} = require('../lib/utils');
const bcrypt = require('bcrypt');

const User = require('../models/user');

passport.use(
    new LocalStratergy({
            usernameField: 'user[email]',
            passwordField: 'user[password]',
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            // Login Stratergy here!
            try {
                let user = await User.where({
                    email: email
                }).fetch({
                    require: false
                });
                if (user) {
                    if (bcrypt.compareSync(password, user.attributes.password)) {
                        let userJson = user.toJSON();
                        const tokens = generateTokens(userJson);
                        return done(null, {
                            user: userJson,
                            ...tokens
                        });
                    }
                }
                throw new Error(constants.error.auth.invalidCredentials);
            } catch (error) {
                return done(error, null);
            }
        }
    )
);

/**
 * @description Generates idToken & refreshToken
 * @param {JSON Object} payload
 */
function generateTokens(payload) {
    const token = jwtUtil.generate({
        ...payload,
        type: jwtUtil.TokenType.ID_TOKEN
    });
    console.log('token', token)
    const refresh_token = jwtUtil.generateRefreshToken({
        ...payload,
        type: jwtUtil.TokenType.REFRESH_TOKEN
    });
    // const twilio_token = twilioUtil.generateToken(payload.email);
    return {
        token,
        refresh_token,
        // twilio_token: twilio_token.token
    };
}

module.exports.generateSignUpToken = function (userJson) {
    console.log(userJson)
    const tokens = generateTokens(userJson);
    return {
        user: userJson,
        ...tokens
    };
};

module.exports.jwtAuth = (req, res, next) =>
    passport.authenticate('jwt', {
        session: false
    }, (err, user, info) => {
        if (err && err.name && err.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(err));
        }
        if (info && info.name && info.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(info));
        }
        if (err || info) return res.serverError(401, ErrorHandler(err || info));
        req.user = user;
        return next();
    })(req, res, next);