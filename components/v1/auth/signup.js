'use strict';
const {
    ErrorHandler
} = require('../../../lib/utils');
const {
    constants
} = require('../../../config');
const User = require('../../../models/user');
const passportMiddleWare = require('../../../middlewares/passport.middleware');


module.exports = async (req, res, next) => {
    try {

        // Checking if email already exists. To speed up fetch we are just getting the count instead of the whole object
        const sameEmailUserCount = await User.where({
            email: req.body.user.email
        }).count();
        if (sameEmailUserCount > 0) {
            return res.serverError(422, ErrorHandler(new Error(constants.error.auth.emailTaken)));
        }

        const body = req.body.user;

        let user = await User.forge(body).save(); // create user in db

        //Generate access tokens
        const data = passportMiddleWare.generateSignUpToken(user.toJSON());
        return res.success(data);
    } catch (error) {
        console.log('error', error)
        return res.serverError(500, ErrorHandler(error));
    }
};