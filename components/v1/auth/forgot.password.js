"use strict";
const { ErrorHandler } = require("../../../lib/utils");
const User = require("../../../models/user");
const uuid = require("uuid");
const moment = require("moment");
const sendGrid = require("../../../lib/utils/sendgrid");
const constants = require("../../../config/constants");


module.exports = async (req, res) => {
    try {
        let user = await User.where({ email: req.body.user.email }).fetch({
            require: false,
        });
        console.log(user);
        if (!user) {
            return res.serverError(
                703,
                ErrorHandler(new Error(constants.error.auth.userNotFound))
            );
        }

        user.reset_password_token = uuid.v4();
        console.log(user.reset_password_token);
        user.reset_password_sent_at = moment.utc().format();

        // let link;
        // link = `${process.env.WEBSITE_USER_URL}/resetpassword?forgot_password_token=${user.reset_password_token}`;

        await sendGrid.send('Base Project - Reset Your Password', 'Base-forgotPassword',
            { name: `${user.toJSON().first_name} ${user.toJSON().last_name}`, reset_password_token: `${user.reset_password_token}` }, [req.body.user.email]);

        console.log(user.id);
        await User.forge({ id: user.id }).save({
            reset_password_token: user.reset_password_token,
            reset_password_sent_at: user.reset_password_sent_at
        });
        return res.success();

    } catch (error) {
        console.log(error);
        return res.serverError(500, ErrorHandler(error));
    }
};
