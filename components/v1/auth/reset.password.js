'use strict';
const {ErrorHandler} = require('../../../lib/utils');
const User = require('../../../models/user');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
    try {

        const body = req.body;
        console.log(body.reset_password_token);

        let user = await User.where({reset_password_token: body.reset_password_token}).fetch({require: false});
       
        if (!user) {
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.invalidPasswordToken)));
        } else {
            if (body.password === body.password_confirmation) {
                await User.forge({id: user.id}).save({
                    reset_password_token: null,
                    reset_password_sent_at: null,
                     password: body.password
                },{patch: true});
            }
            else {
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.passwordNotMatch)));
            }
        }
        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }

};