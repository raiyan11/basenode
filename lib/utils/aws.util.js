const AWS = require('aws-sdk');
var fs = require('fs');
const _ = require('lodash');

AWS.config.update({
    accessKeyId: process.env.AWS_DATASET_ACCESS_KEY,
    secretAccessKey: process.env.AWS_DATASET_SECRET_KEY,
    region: process.env.AWS_REGION
});

const generateUrl = (path, size = {}) => {
    return new Promise((resolve, reject) => {
        try {
            const options = {
                bucket: process.env.AWS_BUCKET,
                key: path,
                edits: size
            };
            const objJsonStr = JSON.stringify(options);
            const objJsonB64 = Buffer.from(objJsonStr).toString('base64');
            const imageUrl = process.env.AWS_CLOUDFRONT_URL + '/' + objJsonB64;
            return resolve(imageUrl);
        } catch (e) {
            return reject(new Error('Something went wrong.'));
        }
    });
};
module.exports = {
    presignUrls: async (folderName,photoPath,extension) => {

        try {
            const s3Urls = {};
            let s3 = new AWS.S3({ signatureVersion: 'v4' });
            const obtainSignedUrls = params =>
                new Promise(resolve =>
                    s3.getSignedUrl('putObject', params, (err, data) => (err ? resolve(false) : resolve(data)))
                );

             const fileStoragePath = `${'uploads/photo'}/${folderName}/${photoPath}${extension}`;
                let params =
                {
                    Bucket: process.env.AWS_BUCKET,
                    Key: fileStoragePath,
                    ACL: 'public-read'
                };
                const signedUrL = await obtainSignedUrls(params);
                if (signedUrL) {
                        s3Urls.presigned_url= signedUrL;
                        s3Urls.photo_path= fileStoragePath;
                       
                     
                }
           

            return s3Urls;
        } catch (error) {
            return error;
        }
    },
    presignarryOfUrl: async (folderName,folderId,token ,extData) => {
        try {
            const s3Urls = [];
            var s3 = new AWS.S3({ signatureVersion: 'v4' });
            const obtainSignedUrls = params =>
                new Promise(resolve =>
                    s3.getSignedUrl('putObject', params, (err, data) => (err ? resolve(false) : resolve(data)))
                );


            for (var i = 0; i < extData.length; i++) {
                console.log(extData[i]);
                const fileStoragePath = `${'uploads'}/${folderName}/${folderId}/${token}_${Date.now().toString()}${extData[i]}`;
                console.log(fileStoragePath);
                var params =
                {
                    Bucket: process.env.AWS_BUCKET,
                    Key: fileStoragePath,
                    ACL: 'public-read'
                };

                const signedUrL = await obtainSignedUrls(params);
                if (signedUrL) {
                    s3Urls.push({
                        signedUrL: signedUrL,
                        publicUrl: `${process.env.AWS_PUBLIC_URL + fileStoragePath}`,
                        publicDbUrl: fileStoragePath
                    });
                }
            }
            //console.log(s3Urls);
            return s3Urls;
        } catch (error) {
            return error;
        }
    },
    presignarryOfUrlQr: async (qrcode, extData) => {
        try {
            const s3Urls = [];
            var s3 = new AWS.S3({ signatureVersion: 'v4' });
            const obtainSignedUrls = params =>
            // console.log(params)
                new Promise(resolve =>
                    s3.getSignedUrl('putObject', params, (err, data) => (err ? resolve(false) : resolve(data)))
                );


            // for (var i = 0; i < extData.length; i++) {
                console.log('extData',extData);
                const fileStoragePath = `${'uploads/temp'}/${qrcode}/${process.env.APP_DOMAIN}_${Date.now().toString()}.${extData}`;
                console.log('file',fileStoragePath);
                var params =
                {
                    Bucket: process.env.AWS_BUCKET,
                    Key: fileStoragePath,
                    ACL: 'public-read'
                };
                console.log(params)
                const signedUrL = await obtainSignedUrls(params);
                console.log('urls',signedUrL);
                if (signedUrL) {
                    s3Urls.push({
                        signedUrL: signedUrL,
                        publicUrl: `${process.env.AWS_BUCKET + fileStoragePath}`,
                        publicDbUrl: fileStoragePath
                    });
                }
            // }
            //console.log(s3Urls);
            return s3Urls;
        } catch (error) {
            return error;
        }
    },
    deleteFile: async (fileStoragePath) => {
        return new Promise((resolve, reject) => {
            try {
                const bucketInstance = new AWS.S3();
                const params = {
                    Bucket: process.env.AWS_BUCKET,
                    Key: fileStoragePath
                };
                bucketInstance.deleteObject(params, function (err, data) {
                    if (data) {
                        return resolve('File deleted successfully');
                    } else {
                        return resolve(false);
                    }
                });
            } catch (error) {
                return reject(error);
            }
        }
        );

    },
    generateVersionNormalUrl: async (url) => {
        if (url && url.length > 0)
            return { original: url, small: url, medium: url };
        else
            return {};
    },
    generateVersionUrls: async (url) => {
        const path = (url.split('.com'))[1].substring(1);
        if (!path) {
            return {};
        } else {
            return {
                original: await generateUrl(path),
                medium: await generateUrl(path, { 'resize': { 'width': 300, 'height': 300 } }),
                small: await generateUrl(path, { 'resize': { 'width': 150, 'height': 150 } })
            };
        }
    },
    uploadBase64Image: async (qrId, base64Image) => {
        return new Promise((resolve, reject) => {
            buf = Buffer.from(base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
            var data = {
                Key: qrId,
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: 'image/png'
            };
            var s3Bucket = new AWS.S3({ params: { Bucket: process.env.AWS_BUCKET } });

            s3Bucket.putObject(data, function (err, data) {
                if (err) {
                    console.log(err);
                    console.log('Error uploading data: ', data);
                    return reject(err)
                } else {
                    console.log('succesfully uploaded the image!');
                    console.log(data)
                    return resolve(data)
                }
            });
        }
        );
    }
};