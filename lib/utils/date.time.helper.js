const { DateTime } = require("luxon");

function getCurrentTimeInMillis() {
    return DateTime.now()
}

//var dt = DateTime.local(2017, 5, 15, 8, 30);  takes any number of arguments, all the way out to milliseconds
//dt = DateTime.fromObject({day: 22, hour: 12, zone: 'America/Los_Angeles', numberingSystem: 'beng'}) create a date time from an object
//DateTime.fromISO("2017-05-15")          //=> May 15, 2017 at midnight
// DateTime.fromISO("2017-05-15T08:30:00") //=> May 15, 2017 at 8:30
//DateTime.now().toString(); //=> '2017-09-14T03:20:34.091-04:00'

/* Getting at components
dt = DateTime.now();
dt.year     //=> 2017
dt.month    //=> 9
dt.day      //=> 14
dt.second   //=> 47
dt.weekday  //=> 4
dt.zoneName     //=> 'America/New_York'
dt.offset       //=> -240
dt.daysInMonth  //=> 30
*/

/* Formatting your DateTime
dt.toLocaleString()      //=> '9/14/2017'
dt.toLocaleString(DateTime.DATETIME_MED) //=> 'September 14, 3:21 AM'
dt.toISO() //=> '2017-09-14T03:21:47.070-04:00'
*/

/* Transforming your DateTime
var dt = DateTime.now();
dt.plus({ hours: 3, minutes: 2 });
dt.minus({ days: 7 });
dt.startOf('day');
dt.endOf('hour');

var dt = DateTime.now();
dt.set({hour: 3}).hour   
*/

/* formatting
var dt = DateTime.now();
var f = {month: 'long', day: 'numeric'};
dt.setLocale('fr').toLocaleString(f)      //=> '14 septembre'
dt.setLocale('en-GB').toLocaleString(f)   //=> '14 September'
dt.setLocale('en-US').toLocaleString(f)  //=> 'September 14'

Info.months('long', {locale: 'fr'}) //=> [ 'janvier', 'février', 'mars', 'avril', ... ]
*/

/* Timezones
DateTime.fromObject({zone: 'America/Los_Angeles'}) // now, but expressed in LA's local time
DateTime.now().setZone("America/Los_Angeles"); // same

UTC
DateTime.utc(2017, 5, 15);
DateTime.utc(); // now, in UTC time zone
DateTime.now().toUTC();
DateTime.utc().toLocal();
*/

/* Durations
The Duration class represents a quantity of time such as "2 hours and 7 minutes". You create them like this:

var dur = Duration.fromObject({ hours: 2, minutes: 7 });
They can be add or subtracted from DateTimes like this:

dt.plus(dur);
They have getters just like DateTime:

dur.hours   //=> 2
dur.minutes //=> 7
dur.seconds //=> 0
And some other useful stuff:

dur.as('seconds') //=> 7620
dur.toObject()    //=> { hours: 2, minutes: 7 }
dur.toISO()       //=> 'PT2H7M'
*/

/* Intervals
now = DateTime.now();
later = DateTime.local(2020, 10, 12);
i = Interval.fromDateTimes(now, later);

i.length()                             //=> 97098768468
i.length('years')                //=> 3.0762420239726027
i.contains(DateTime.local(2019))       //=> true

i.toISO()       //=> '2017-09-14T04:07:11.532-04:00/2020-10-12T00:00:00.000-04:00'
i.toString()    //=> '[2017-09-14T04:07:11.532-04:00 – 2020-10-12T00:00:00.000-04:00)
*/

module.exports = { getCurrentTimeInMillis};
