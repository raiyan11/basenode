const Joi = require('joi');


//Auth validations

module.exports.signUp = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
    user_name: Joi.string().required(),
    photo_url: Joi.string(),
    bio: Joi.string(),
    first_name: Joi.string(),
    last_name: Joi.string(),
    session_info: Joi.object(),
    width: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    height: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    })
});

module.exports.signUser = Joi.object({
    email: Joi.string().email().required().messages({
        "string.base": "Email is invalid",
        "string.empty": "Email cannot be an empty",
        "any.required": "Email is required",
      }),
    password: Joi.string().required()
});
