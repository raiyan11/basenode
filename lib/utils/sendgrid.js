'use strict';
const Email = require('email-templates');
const template = new Email({
  juiceResources: {
    preserveImportant: true,
    webResources: {
      images: false
    }
  }
});

const path = require('path');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const templates = {
  'Base-forgotPassword': 'd-e7fa7fbe136145a2836c49c680931ca8'
}

class SendGridEmail {
	static send(
    subject,
    templatePath,
    payload,
    toAddresses = [],
    ccAddresses = [],
    bccAddresses = []
  ) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    return new Promise(async (resolve, reject) => {
      payload.subject = subject;
      if (templates[templatePath]) {
        sgMail
          .send({
            to: toAddresses,
            cc: ccAddresses,
            bcc: bccAddresses,
            from: process.env.SENDGRID_FROM,
            // subject,
            templateId: templates[templatePath],
            dynamic_template_data: {
              payload
            }
          })
          .then((r) => resolve(true))
          .catch((e) => {
            console.log(e)
            reject(e)
          });
      } else {
        const html = await template.render(
          path.join(__dirname, "../../views", templatePath),
          payload
        );
        sgMail
          .send({
            to: toAddresses,
            cc: ccAddresses,
            from: process.env.SENDGRID_FROM,
            subject,
            html
          })
          .then(r => resolve(true))
          .catch(e => reject(e));
      }
    });
  }
}

module.exports = SendGridEmail;