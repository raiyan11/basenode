
exports.up = function(knex) {
    return knex.schema.createTable('users', t => {
         t.increments();
        t.string('first_name');
        t.string('last_name');
        t.string('email').unique();
        t.string('phone');
        t.string('user_name').unique();
        t.string('password');
        t.string('timezone');
        t.string('photo_url');
        t.string('width');
        t.string('height');
        t.boolean ('verified').defaultTo(false);
        t.string ('verification_token');
        t.string ('reset_password_token');
        t.dateTime ('reset_password_sent_at');
        t.dateTime ('terms_agreed_at');
        t.enu('role', ['user','admin'], {useNative: false}).defaultTo('user');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index (['email'], 'index_user_on_email');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('users');
};
