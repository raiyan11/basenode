
exports.up = function(knex) {
    return knex.schema.createTable('tasks', t => {
        t.increments();
        t.string('title');
        t.text('description');
        t.string ('timezone');
        t.string('scheduled_at')
        t.string ('completed_at');
        t.enu('status', ['pending','completed','cancelled'], {useNative: false}).defaultTo('pending');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index (['title'], 'index_task_on_title');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('task');
};
