
exports.up = function(knex) {
    return knex.schema.createTable('user_extras', t => {
        t.increments();
        t.text('bio');
        t.bigInteger('age');
        t.bigInteger('user_id').references('id').inTable('users');
        t.enu('sex', ['male','female','other'], {useNative: false}).defaultTo('other');
        t.enu('privacy_settings', ['public','private'], {useNative: false}).defaultTo('public');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index (['user_id'], 'index_user_extra_on_user_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('user_extra');
};
