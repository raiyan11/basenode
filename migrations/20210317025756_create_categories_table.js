
exports.up = function(knex) {
    return knex.schema.createTable('categories', t => {
        t.increments();
        t.string('name');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index (['name'], 'index_name_on_category');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('category');
};
