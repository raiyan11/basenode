exports.up = function (knex) {
    return knex.schema.createTable('attachments', t => {
        t.increments();
        t.string('entity_type');
        t.bigInteger('entity_id');
        t.decimal('width');
        t.decimal('height');
        t.string('url');
        t.string('sort');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index(['entity_type', 'entity_id'], 'index_attachments_on_entity_type_and_entity_id');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('attachments');
};
