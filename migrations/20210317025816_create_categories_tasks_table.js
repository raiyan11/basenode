
exports.up = function(knex) {
    return knex.schema.createTable('categories_tasks', t => {
        t.increments();
        t.string('name');
        t.bigInteger('category_id').references('id').inTable('categories');
        t.bigInteger('task_id').references('id').inTable('tasks');
        t.enu('active_status',['active','inactive','deleted','hidden'],{ useNative: false}).defaultTo('active');
        t.timestamps();
        t.index (['category_id','task_id'], 'category_id_task_id_on_categories_tasks');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('categories_tasks');
};
